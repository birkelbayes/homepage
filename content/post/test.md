+++
title = "Relaunch"

author = ["Robert Birkelbach"]
lastmod = 2018-05-24T15:55:25+02:00
draft = false

tags = ["website", "org-mode", "hugo"]
summary = "I took the GDPR as an opportunity to relaunch the website. It's build with hugo and the academic theme. I use ox-hugo to export posts written in org-mode to markdown."  
+++




# Relaunch {#relaunch}

I took the GDPR as an opportunity to relaunch the website. It's build with [hugo](https://gohugo.io) and the [academic theme](https://github.com/gcushen/hugo-academic). I use [ox-hugo](https://github.com/kaushalmodi/ox-hugo) to export posts written in [org-mode](https://www.orgmode.org) to markdown.
