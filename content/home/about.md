+++
# About/Biography widget.
widget = "about"
active = true
date = 2018-05-18

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Analytical Sociology",
    "Social Capital",
    "Educational Sociology",
	"Migration and Integration",
	"Norms",
	"Causal Inference",
	"Bayesian Statistics",
	"Social Network Analysis"
  ]

# List your qualifications (such as academic degrees).

[[education.courses]]
  course = "PhD student, since 2013"
  institution = "University of Cologne"

[[education.courses]]
  course = "Graduate School SOCLIFE "
  institution = "University of Cologne"
  year = 2016
[[education.courses]]
  course = "Msc Methodology and Statistics of Social and Behavioral Science"
  institution = "Utrecht University"
  year = 2013

[[education.courses]]
  course = "BA Sociology"
  institution = "Mannheim University"
  year = 2011
 
+++

# Biography

I'm a social scientist/statistician and currently work at the [research data center](https://fdz.dzhw.eu) of the [German Centre for Higher Education Research and Science Studies](https://www.dzhw.eu). In a team with software developers I work on the [metadata management system](https://metadata.fdz.dzhw.eu) of the research data center at DZHW GmbH where I'm mainly responsible for software testing and metadata management. Check out our [github repository](https://github.com/dzhw/metadatamanagement). Besides that I prepare data to disseminate them in the form of scientific use files and campus use files.

Previously I've been a data scientist at [respondi AG](https://www.respondi.com) doing market research (2017-2018). I was a researcher at [Clemens Kroneberg's](http://www.kroneberg.eu) chair [Sociology I](http://www.iss-wiso.uni-koeln.de/institut/professuren/soziologie-i-prof-dr-clemens-kroneberg/) from 2016 to 2017 and gave a course on analytical sociology for Bachelor students. From 2013 to 2016 I was a PhD student at the SOCLIFE graduate school where I started my PhD on Coleman's intergenerational closure hypothesis. I have a Master of Science in Methodology and Statistics of Social and Behavioral Science from Utrecht University and a Bachelor in Sociology from Mannheim University.  
