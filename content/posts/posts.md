+++
author = ["Robert Birkelbach"]
lastmod = 2018-05-24T15:36:26+02:00
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

## Table of Contents

- [Relaunch](#relaunch)

</div>
<!--endtoc-->



## <span class="todo DONE_">DONE </span> Relaunch {#relaunch}

I took the GDPR as an opportunity to relaunch the website. It's build with hugo and the academic theme. I use ox-hugo to export posts written in org-mode to markdown.
